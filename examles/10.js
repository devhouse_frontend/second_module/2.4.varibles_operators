console.log(true || true);   // true
console.log(false || true);  // true
console.log(5 > 2 || 5 < 2);  // true
console.log(false || false); // false
// результат ИЛИ может быть ложь только если все операнды ложь
console.log(false || false || true); // true

console.log(true && true);   // true
console.log(false && true);  // false
console.log(5 > 2 && 5 < 2);  // false
console.log(false && false); // false
// результат И может быть истинной только если все операнды истина
console.log(false && false && true); // false

console.log(!true); // false
console.log(!0); // true
console.log(!!0); // false
console.log(Boolean(0)); // false

result = value1 || value2 || value3;
// Тут мы проходимся слева на право по всем выражениям value и вычисляем их булево значение, первое значение, которое окажется истинным будет записано в result.
result = value1 && value2 && value3;
// В этом случае будет записано первое ложное value, если все истины – вернет последний операнд.

let fir_val = null;
let sec_val = "Second";

let result = fir_val || sec_val || "Nothing";

console.log(result); // выбирается "Second" – первое истинное значение