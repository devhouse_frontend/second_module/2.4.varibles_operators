// Явное и неявное приведение типов
// Явное приведение типов
let x = '123' // строка “123”
x = Number(x)
console.log(x); // 123
console.log(typeof x); // number

// Неявное приведение:
let a = 2     // число 2
let y = '123' // строка 123
let z = a + y
console.log(z); // строка 2123
console.log(typeof z); // string