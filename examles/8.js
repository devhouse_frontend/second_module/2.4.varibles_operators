// Сравнение чисел
console.log( 9 == 9 ); // true (верно)
console.log( 9 != 9 ); // false (неверно)
console.log( 9 > 9 );  // false (неверно)
console.log( 9 > -9 ); // true (верно)

// результат сравнения - логическая переменная
let result = 10 > 9; // результат сравнения присваивается переменной result
console.log( result ); // true

// сравнение строк происходит посимвольно
console.log( 'y' > 'a' ); // true
console.log( 'строка' < 'строки' ); // true а < и
console.log( 'полка' > 'полк' ); // true первые части строк одинаковы, но полка длиннее
console.log( 'полк' > 'папирус' ); // true о > а

console.log( '9' > 8 ); // true, строка '9' становится числом 9
console.log( '09' == 9 ); // true, строка '01' становится числом 1
console.log( true == 9 ); // false, true приводится к 1
console.log( false == 0 ); // true, false приводится к 0
console.log( true == 1 ); // true
console.log('9' !== 9); // true, так как разный тип
console.log( true === 9); // false, так как разный тип

// null и undefined разных типов
console.log( null === undefined ); // false
console.log( null == undefined ); // true