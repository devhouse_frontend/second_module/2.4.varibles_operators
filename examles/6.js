let x = 10;
console.log(Boolean(x)) //  true
let y = 0;
console.log(Boolean(y)) // false
let z = -2;
console.log(Boolean(z)) //  true
let q = null;
console.log(Boolean(q)) // false
let w;
console.log(Boolean(w)); //undefined – false
let empty_str = "";
console.log(Boolean(empty_str))// false
let r = "some str";
console.log(Boolean(r))//true