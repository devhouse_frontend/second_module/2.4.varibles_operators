// Запись строк

// используем одинарные кавычки
let simple_string = 'some string'
// используем двойные кавычки
let another_string = "some string"
// some string
console.log(simple_string);
console.log(another_string);